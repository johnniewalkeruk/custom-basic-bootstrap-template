#Creating a Custom Basic Bootstrap Page Template 

This is a project to record the step-by-step creation of a basic, modern, web page with [Bootstrap3](http://getbootstrap.com/) (the front-end framework).

You can download a Basic template for a Bootstrap page on the [getbootstrap.com web site](http://getbootstrap.com/getting-started/#examples).  
This project has slightly different constraints and so results in a similar, but different, page.

This accompanying write-up explains the reasoning behind each aspect of our page.

View this project's source code to see the final directory structure of this page and the files in the libraries that it depends on.

The steps are based on:

* Chapter 1 of [Jump Start Bootstrap (2014) by Syed Fazle Rahman.](http://www.amazon.co.uk/Jump-Start-Bootstrap-Fazle-Rahman/dp/0992279437), and 
* Chapter 1 of [Introducing HTML5, Second Edition (2012), by Bruce Lawson and Remy Sharp](http://www.amazon.co.uk/Introducing-HTML5-Voices-That-Matter/dp/0321784421), and
* [Yahoo's Best Practices for Speeding Up Your Web Site](https://developer.yahoo.com/performance/rules.html#js_bottom) and [High Performance Web Sites (2007), by Steve Souders](http://www.amazon.co.uk/High-Performance-Web-Sites-Essential/dp/0596529309).
* Some of my own experience.

## Browser Support

We aim to be able to fully support legacy browsers from IE8 and above.

## Steps

### Create a project and a cache-busting directory for Bootstrap

Create a directory for your project with a [Document Root directory](http://httpd.apache.org/docs/current/mod/core.html#documentroot).

Inside that Document Root create a directory to house all of our 3rd party front-end libraries. Call it `doc-resources/shared` (as in 'shared document resources').   

Inside our 'shared document resources' directory, create a directory for Bootstrap that contains the version number:

    doc-resources/shared/bootstrap/3.2.0
    
We ensure the version number is present in paths to our document resources such as CSS, JavaScript, fonts and image files. 
This means that we can aid performance by setting a far-future cachetime for such files on the server whilst also forcing a download of newer versions should they become available. 
This is a tip from Yahoo. See [Yahoo's Best Practices for Speeding Up Your Web Site: Add an Expires or a Cache-Control Header](https://developer.yahoo.com/performance/rules.html#expires)    

### Get Bootstrap3 

Ensure the Bootstrap3 css, fonts and js files are placed into our 'shared document resources' directory.

* Get the latest 3.x.x Bootstrap package from [http://getbootstrap.com](http://getbootstrap.com). For now, just get the Precompiled Bootstrap. 
* Copy the `css`, `fonts` and `js` directories from the downloaded Bootstrap package into the project in:
   
    doc-resources/shared/bootstrap/3.2.0/
    
The `css` files refer to the `fonts` files using file-relative links. Therefore it is not critical where the Bootstrap files _library_ is placed as long as the directory structure within that library is maintained.

### Create the File for the Web Page

Add an empty file called `index.html` to the Document Root directory. 

You could use any valid name for the file. I chose that name because it is for the home page of this project and `index.html` is the default value for the [DirectoryIndex](http://httpd.apache.org/docs/current/mod/mod_dir.html#directoryindex) setting on my Apache server.

### Specify the Document Type Definition (DTD) in index.html

Edit the `index.html` file to ensure the [DTD Declaration](http://www.w3.org/QA/Tips/Doctype) is set at the top. 
This ensures that whatever processes the page knows it is an HTML5 page.

    <!DOCTYPE html>

### Add the Root HTML Element

Continue to edit the `index.html` page. Add the root `<html>` element that will surround all the other HTML elements of the page.

Ensure that the `lang` attribute is set explicitly. Amongst other things, this tells screenreader software (for the visually impaired) which primary language the page is written in. 
That way they don't have to guess whether to pronounce the word 'six' in French or English. See page 4 of [Introducing HTML5, Second Edition (2012)](http://www.amazon.co.uk/Introducing-HTML5-Voices-That-Matter/dp/0321784421)

    <!DOCTYPE html>
    <html lang="en">
    </html>
    
### Add the Minimal Amount of Content Using the Body Element

The [HTML Body Element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/body) (`<body>`) represents the content of an HTML document.
Let's fill it with some arbitrary text so we have something to see if we view the page in a browser.
      
    <!DOCTYPE html>
    <html lang="en">
        <body>
            <h1>Hello, world!</h1>
        </body>
    </html>

### Add the Minimal Amount of Metadata Using the Head Element 

The [HTML Head Element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/head) (`<head>`) contains general information (metadata) about the document. 

The `<head>` element of a 'minimal but maintainable web page' should, at least, 

* Explicitly define the document's character encoding, and
* Contain a page `title`

#### Defining the Character Encoding
A text file is a stream of computer bytes that can be interpreted in a multitude of ways. The page's letters and digits could be coded in any one of a variety of different 'Character Sets'. Therefore, it is good practice to add a `meta` tag to explain which character encoding (`charset`) has been used in the document. The [UTF-8 character encoding](http://www.utf-8.com/) is a good choice for most web sites. 
For more information about character sets, see [The Minimum Every Software Developer Must Know About Character Sets](http://www.joelonsoftware.com/articles/Unicode.html).   

#### Page Title
Page titles are used to represent 'open web pages' as labels in browser [tabs](http://en.wikipedia.org/wiki/Tab_%28GUI%29) and [windows](http://en.wikipedia.org/wiki/Window_%28computing%29) on your device's [taskbar](http://en.wikipedia.org/wiki/Taskbar) and [toolbars](http://en.wikipedia.org/wiki/Toolbar). You can set a page's title using the `title` HTML element.

So, let's add the `head` element with these two pieces of metadata: 
 
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <title>My Modern Web Page</title>
        </head>
        <body>
            <h1>Hello, world!</h1>
        </body>
    </html>

### Link Bootstrap's `css` file to our `index.html` file

Add a `<link>` element into the document head to point to Bootstrap's css file.
 
There are a number of versions of this file in our Bootstrap's `css` directory. We point to `bootstrap.css` during development. This is the most readable (but least optimised) version. 

Add:

    <link href="doc-resources/shared/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet">
    
To give:    

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <title>My Modern Web Page</title>
            
            <!-- Bootstrap -->
            <link href="doc-resources/shared/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet">
        </head>
        <body>
            <h1>Hello, world!</h1>
        </body>
    </html>
    
   
### Choose a version of jQuery to use with Bootstrap

Bootstrap uses the jQuery JavaScript library. JQuery is an altogether separate project which evolves at a different pace. 
So, as web developers, it is our responsibility to ensure that we have it available to our project's code.

#### Determine which version/s of jQuery Bootstrap depends on? 

The text on the [getbootstrap.com](http://getbootstrap.com/getting-started/#jquery-required) site says:

> jQuery required
> Please note that all JavaScript plugins require jQuery to be included, as shown in the starter template. 
> Consult our bower.json to see which versions of jQuery are supported.
  
If we follow their link to the 'bower.json' we see some, machine-readable, [JSON](http://json.org/) code which describes certain attributes of the latest version of the Bootstrap project.

This information is bound to change over time. But, currently, the notable sections are:

    {
          "name": "bootstrap",
          ...
          "version": "3.2.0",
          ...
          "dependencies": {
            "jquery": ">= 1.9.0"
          ...
    }

This implies that version 3.2.0 of Bootstrap depends on any version of jQuery that is greater than or equal to version 1.9.0.

Let's have a quick check of the comment text at the top of one of our project's Bootstrap files.

For example, in:

    `doc-resources/shared/bootstrap/3.2.0/css/bootstrap.css` 

We see:

    /*!
     * Bootstrap v3.2.0 (http://getbootstrap.com)
     * Copyright 2011-2014 Twitter, Inc.
     * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
     */

This reassures us that our project has version 3.2.0 of Bootstrap and, according to the info in the 'bower.json' (mentioned above), we can use jQuery version 1.9.0 or greater.
 
#### Determine which version/s of jQuery we want?

The jQuery project's [Browser Support page](http://jquery.com/browser-support/) indicates that versions greater than version 2 do not support Internet Explorer 8. 
So, we should use the latest version of jQuery that is below version 2.

Download the latest uncompressed jQuery Core 1.x (`jquery-1.11.1.js`) and paste it into the project's `/js` directory at:

    doc-resources/shared/jquery/    

### Link jQuery to the `index.html` file

Insert the link to the jQuery at the bottom of the `<body>` tag. 

By placing the JavaScript indside the `<body>` element (and not in the `<head>` element), we ensure that jQuery is loaded _after_ all the HTML contents are loaded. This makes for a faster user experience. For more information about this see [Yahoo's Best Practices for Speeding Up Your Web Site: Put Scripts at the Bottom](https://developer.yahoo.com/performance/rules.html#js_bottom) and [High Performance Web Sites (2007), by Steve Souders](http://www.amazon.co.uk/High-Performance-Web-Sites-Essential/dp/0596529309).
    
Add:

    <script src="doc-resources/shared/jquery/jquery-1.11.1.js"></script> 
    
To give:

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <title>My Modern Web Page</title>
            
            <!-- Bootstrap -->
            <link href="doc-resources/shared/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet">
        </head>
        <body>
            <h1>Hello, world!</h1>
            
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="doc-resources/shared/jquery/jquery-1.11.1.js"></script>
        </body>
    </html>    

### Link Bootstrap's JavaScript to the `index.html` file

Add a line to include Bootstrap's JavaScript file in the web page. Place it just below the link to jQuery.
 
    <script src="doc-resources/shared/bootstrap/3.2.0/js/bootstrap.js"></script>   

### Force Internet Explorer (IE) to use the latest rendering engine

Sometimes Internet Explorer may run in _compatibility mode_. Add a meta tag to tell IE to use the latest rendering engine to render this page. Older rendering engines do not support all properties of CSS.

Add this line to the `<head>`:

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

### Set the `viewport` to scale our content to 100% of the device's screen

Tell the browser to scale our page's content to the size of window space available by placing another meta tag into the `<head>`:

    <meta name="viewport" content="width=device-width, initial-scale=1">

Now our page should look like this:

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>My Modern Web Page</title>
            
            <!-- Bootstrap -->
            <link href="doc-resources/shared/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet">
        </head>
        <body>
            <h1>Hello, world!</h1>
            
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="doc-resources/shared/jquery/jquery-1.11.1.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="doc-resources/shared/bootstrap/3.2.0/js/bootstrap.js"></script>
        </body>
    </html>

### Force old versions of Internet Explorer to acknowledge newer HTML5 elements and CSS3

Add some conditional [polyfills](https://remysharp.com/2010/10/08/what-is-a-polyfill) to force old versions of Internet Explorer to acknowledge the newer HTML5 elements and CSS3. See Page 10 of [Introducing HTML5, Second Edition (2012), by Bruce Lawson and Remy Sharp](http://www.amazon.co.uk/Introducing-HTML5-Voices-That-Matter/dp/0321784421) for more details.

I prefer to host these files on my server rather than using a content delivery network (CDN). This is because I may be using this template in admin panels, on an in-house web site or within an intranet and don't want the guys at maxcdn logging all my page views.  

* Download `html5shiv` from https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js and place it into our `doc-resources/shared/html5shiv/3.7.2` directory (with the cache-breaking version number in the path). 
* Download `respond` from https://oss.maxcdn.com/respond/1.4.2/respond.min.js and place it into our `doc-resources/shared/respond/1.4.2` directory (again, with the cache-breaking version number in the path).

```
#!html

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="doc-resources/shared/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="doc-resources/shared/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
```

### View the Complete Page

Now the page is complete. It should look like this:


    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>My Modern Web Page</title>
            
            <!-- Bootstrap -->
            <link href="doc-resources/shared/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet">
            
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
                <script src="doc-resources/shared/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="doc-resources/shared/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
                
        </head>
        <body>
            <h1>Hello, world!</h1>
            
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="doc-resources/shared/jquery/jquery-1.11.1.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="doc-resources/shared/bootstrap/3.2.0/js/bootstrap.js"></script>
        </body>
    </html>

### Test the Page

Open the `index.html` in Google Chrome. Right-click on the page to bring up in the [context menu](http://en.wikipedia.org/wiki/Context_menu) and select `Inspect Element`. When the 'Developer Tools' pane opens up, click on the `Console` tab. If there are no errors, all the JavaScript files have loaded properly. Then click on the `Network` tab. If there are no 404 errors it means that all the CSS files loaded properly. 

In the spirit of test-driven development, I sometimes break the code on purpose (by temporarily suffixing blurb onto the links) to see what it looks like in error. Then, refresh and repeat to see how it looks once I've fixed it.


### See How Our Template Differs From the Official Bootstrap One

Let's use a [diff tool](http://en.wikipedia.org/wiki/Diff_utility) to list the difference between our basic template page and the one offered on [getbootstrap.com](http://getbootstrap.com/getting-started/#template)

Run the `diff` command on a Linux, Mac or [GitBash](http://msysgit.github.io/) terminal:    
    # diff -u -0 official-bootstrap-basic-template-v3.2.0.html index.html   

Will result in:

    
    --- official-bootstrap-basic-template-v3.2.0.html	Mon Oct 13 16:45:44 2014
    +++ index.html	Mon Oct 13 17:02:31 2014
    @@ -7 +7 @@
    -        <title>Bootstrap 101 Template</title>
    +        <title>My Modern Web Page</title>
    @@ -10 +10 @@
    -        <link href="css/bootstrap.min.css" rel="stylesheet">
    +        <link href="doc-resources/shared/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet">
    @@ -15,2 +15,2 @@
    -            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    -            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    +            <script src="doc-resources/shared/html5shiv/3.7.2/html5shiv.min.js"></script>
    +            <script src="doc-resources/shared/respond/1.4.2/respond.min.js"></script>
    @@ -23 +23 @@
    -        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    +        <script src="doc-resources/shared/jquery/jquery-1.11.1.js"></script>
    @@ -25 +25 @@
    -        <script src="js/bootstrap.min.js"></script>
    +        <script src="doc-resources/shared/bootstrap/3.2.0/js/bootstrap.js"></script>

### Grab Some Advanced Templates

Now that we have this list of differences, we can grab some [more advanced templates](http://getbootstrap.com/getting-started/#examples) from the getbootstrap.com web site and use this as a checklist of basic find/replace edits that need to be made to ensure they match our set-up.

